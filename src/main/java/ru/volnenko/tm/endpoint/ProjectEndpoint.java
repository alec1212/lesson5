package ru.volnenko.tm.endpoint;

import ru.volnenko.tm.entity.Project;
import ru.volnenko.tm.service.ProjectService;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint {

    private ProjectService projectService;

    public ProjectEndpoint() {
    }

    public ProjectEndpoint(ProjectService projectService) {
        this.projectService = projectService;
    }

    @WebMethod
    public Project create(String name) {
        return projectService.create(name);
    }

//    @WebMethod
//    public Project create(String name, String description) {
//        return projectService.create(name, description);
//    }

    @WebMethod
    public Project update(Long id, String name, String description) {
        return projectService.update(id, name, description);
    }

    @WebMethod
    public void clear() {
        projectService.clear();
    }

    @WebMethod
    public Project findByIndex(int index) {
        return projectService.findByIndex(index);
    }

    @WebMethod
    public Project findByName(String name) {
        return projectService.findByName(name);
    }
    @WebMethod
    public Project findById(Long id) {
        return projectService.findById(id);
    }
    @WebMethod
    public Project removeByIndex(int index) {
        return projectService.removeByIndex(index);
    }
    @WebMethod
    public Project removeById(Long id) {
        return projectService.removeById(id);
    }
    @WebMethod
    public Project removeByName(String name) {
        return projectService.removeByName(name);
    }
    @WebMethod
    public List<Project> findAll() {
        return projectService.findAll();
    }

}
