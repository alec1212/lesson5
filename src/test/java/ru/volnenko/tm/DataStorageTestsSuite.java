package ru.volnenko.tm;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.volnenko.tm.marker.IDataStorageTests;
import ru.volnenko.tm.service.ProjectService;
import ru.volnenko.tm.service.ProjectServiceTest;
import ru.volnenko.tm.service.StorageServiceTest;

@RunWith(Categories.class)
@Categories.IncludeCategory(IDataStorageTests.class)
@Suite.SuiteClasses({ProjectServiceTest.class, StorageServiceTest.class})
public class DataStorageTestsSuite {
}
