package ru.volnenko.tm.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volnenko.tm.entity.Project;
import ru.volnenko.tm.marker.IDataMemoryTests;
import ru.volnenko.tm.marker.IDataStorageTests;
import ru.volnenko.tm.repository.ProjectRepository;

public class ProjectServiceTest {

    private final ProjectRepository projectRepository =  new ProjectRepository();
    ProjectService projectService = new ProjectService(projectRepository);


    @Test
    @Category(IDataMemoryTests.class)
    public void CreateTest()
    {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectService.create("ProjecTest1");
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        Assert.assertNull(projectRepository.findByName("ProjecTest2"));
    }
    @Test
    @Category(IDataMemoryTests.class)
    public void UpdateTest()
    {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectService.create("ProjecTest1");
        Assert.assertNotNull(projectRepository.findByName("ProjecTest1"));
        Project project1 = projectRepository.findByName("ProjecTest1");
        projectService.update(project1.getId(), "NewProjectTest1Name", "NewProjectTest1Description");
        Assert.assertNotNull(projectRepository.findByName("NewProjectTest1Name"));
        Project project2 = projectRepository.findByName("NewProjectTest1Name");
        Assert.assertEquals(project1.getId(), project2.getId());
        Assert.assertEquals("NewProjectTest1Description", project2.getDescription());
    }
}
